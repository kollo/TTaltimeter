' Hoehenanzeige.

' Adaptiert fuer X11-Basic  (c)  2008 Markus Hoffmann

sizew ,320,240
rot=get_color(65535,0,0)
gelb=get_color(65535,65535,0)
orange=get_color(65535,34000,0)
weiss=get_color(65535,65535,65535)
schwarz=get_color(0,0,0)
blau=get_color(0,0,65535)
gruen=get_color(0,65535,0)
grau=get_color(20000,20000,20000)
magenta=get_color(65535,0,65535)
lila=get_color(20000,0,20000)
helveticanormfont$="-*-helvetica-bold-r-normal-*-14-*-iso8859-*"
helveticasmallfont$="-*-helvetica-medium-r-normal-*-10-*-iso8859-*"

ziff()=[0x7b,0x42,0x37,0x67,0x4e,0x6d,0x7d,0x43,0x7f,0x6f]

scale=0.5

tx=60
ty=100
tw=32
th=40

color schwarz
pbox 0,0,320,240

if exist("/var/run/gpsfeed")
  open "I",#1,"/var/run/gpsfeed"
else
  open "I",#1,"/home/hoffmann/bas/gps/log/nmea-11.08.2008.log"
endif

' Draw the sky
color grau
circle 260,180,50*abs(90-80)/90
circle 260,180,50*abs(90-45)/90
circle 260,180,50*abs(90-10)/90
circle 260,180,50*abs(90-0)/90
plot 260,180


deftext 0
color blau
text 0,240-6,"V.1.00 (c) Markus Hoffmann 2008"
color grau
line 0,120,320,120
line 200,0,200,240
color rot
deftext 1
text 260-50-4,180,"W"
text 260+50-4,180,"E"
text 260-4,180-50,"N"
text 260-4,180+50,"S"
' put 200+16,2*16,sun_bmp$

get 201,121,119,119,sky$
do
  lineinput #1,t$
  if left$(t$,6)="$GPGGA"
    for i=0 to 9
      split t$,",",0,a$,t$
      if i=0
      else if i=1
        time=val(a$)
      else if i=2
        a=val(a$)
        lat=int(a/100)
	a=a-lat*100
	lat=lat+a/60
      else if i=3
        latsign=(a$="S")
      else if i=4
        a=val(a$)
        lon=int(a/100)
	a=a-lon*100
	lon=lon+a/60
      else if i=5
        lonsign=(a$="W")
      else if i=6
        quality=val(a$)
      else if i=7
        numsat=val(a$)
      else if i=8
        dilution=val(a$)
      else if i=9
        alt=val(a$)
      else
        print a$
      endif
    next i
    if quality    
      hour=int(time/10000)
      minute=int((time-hour*10000)/100)
      second=int((time-hour*10000-minute*100))
      lat=(latsign*2+1)*lat
      lon=(lonsign*2+1)*lon
      @displayit 
    endif
  else
'    print "Unknown: "+left$(t$,16)
  endif    
loop

quit

procedure displayit
    deftext 1
    color gelb
    setfont helveticanormfont$
    text 0,16,str$(hour,2,2,1)+":"+str$(minute,2,2,1)+":"+str$(second,2,2,1)+" +"+str$(zone)+"h  "+str$(day,2,2,1)+"."+str$(month,2,2,1)+"."+str$(year,4,4,1)

    color weiss
    text 0,49,@breite$(lat)+" "+@laenge$(lon)
    color magenta
    text 0,64,"alt: "+str$(alt)+" m "
    color gruen
    text 0,79,"dist: "+str$(distance)+" m "
    tendenz=alt-oldalt
    color schwarz
    pbox tx,ty,tx+tw,ty+th
    pbox 100,100,100+40*4,152
    color magenta
    ltext 100,100,str$(alt,4,4)
    color gruen
    box tx-1,100-2,100+40*4+1,152+1
    color rot
    if lt=0
      line tx,ty+th/2-3,tx+tw,ty+th/2
      line tx,ty+th/2+3,tx+tw,ty+th/2
    else if lt>0.01
      line tx,ty+th/2-3,tx+tw,ty
      line tx,ty+th/2+3,tx+tw,ty
    else if lt<-0.01
      line tx,ty+th/2-3,tx+tw,ty+th
      line tx,ty+th/2+3,tx+tw,ty+th
    endif
    color gelb
    lt=0.95*lt+0.05*tendenz
    if tendenz=0
      line tx,ty+th/2-3,tx+tw,ty+th/2
      line tx,ty+th/2+3,tx+tw,ty+th/2
    else if tendenz>0
      line tx,ty+th/2-3,tx+tw,ty
      line tx,ty+th/2+3,tx+tw,ty
    else
      line tx,ty+th/2-3,tx+tw,ty+th
      line tx,ty+th/2+3,tx+tw,ty+th
    endif
    oldalt=alt

  deftext 0
  setfont helveticasmallfont$
  t$=@conv$(str$(alt,5,5))
  groesse=scale*1.5
  @puts7(40,170,t$)

  vsync

' if mousek
'  savewindow "a.bmp"
'  quit
' endif
return


function breite$(x)
  local posx$
  IF x>0
    posx$="N"
  ELSE
    posx$="S"
  ENDIF
  x=ABS(x)
  posx$=posx$+RIGHT$("00"+str$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+str$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+str$(INT(x)),2)+"."
  x=x-INT(x)
  x=x*10
  posx$=posx$+str$(INT(x))
  return posx$
endfunc
function laenge$(x)
  local posx$
  IF x>0
    posx$="E"
  ELSE
    posx$="W"
  ENDIF
  x=ABS(x)
  posx$=posx$+RIGHT$("000"+str$(INT(x)),3)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+str$(INT(x)),2)+":"
  x=x-INT(x)
  x=x*60
  posx$=posx$+RIGHT$("00"+str$(INT(x)),2)+"."
  x=x-INT(x)
  x=x*10
  posx$=posx$+str$(INT(x))
  return posx$
endfunc
procedure puts7(x,y,c$)
  local i
  for i=0 to len(c$)-1
    @put7(x,y,groesse,peek(varptr(c$)+i))
    add x,32*groesse
    if x>640*scale
    x=0
    add y,64*groesse
    endif
  next i  
return

procedure put7(x,y,s,c)
  local i
  color schwarz
  pbox x,y,x+s*32,y+s*64
d()=[3,3,29,3;29,3,29,29;3,32,29,32;3,3,3,29;3,32,3,61;3,61,29,61;29,61,29,32;34,64,35,64]
for i=0 to 7
  if btst(c,i)
    color rot
  else
    color grau
  endif
  defline ,4*s,2
  line x+d(i,0)*s*0.8,y+d(i,1)*s*0.6,x+d(i,2)*s*0.8,y+d(i,3)*s*0.6
next i
return

function conv$(t$)
  local i,a$
  a$=""
  for i=0 to len(t$)-1
    if peek(varptr(t$)+i)=asc(".")
      if len(a$)
        poke varptr(a$)+len(a$)-1,peek(varptr(a$)+len(a$)-1) or 0x80
      else
        a$=chr$(ziff(0) or 0x80)
      endif
    else  if peek(varptr(t$)+i)=asc(" ")
      a$=a$+chr$(0)
    else  if peek(varptr(t$)+i)=asc("-")
      a$=a$+chr$(4)
    else  if peek(varptr(t$)+i)=asc("e")
      a$=a$+chr$(0x3d)
    else  if peek(varptr(t$)+i)=asc("E")
      a$=a$+chr$(0x3d)
    else  if peek(varptr(t$)+i)=asc("f")
      a$=a$+chr$(0x1d)
    else  if peek(varptr(t$)+i)=asc("a")
      a$=a$+chr$(0x5f)
   else
      a$=a$+chr$(ziff(peek(varptr(t$)+i)-asc("0")))
    endif
  next i
  return a$
endfunction
